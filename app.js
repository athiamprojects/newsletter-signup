const express = require("express");
const request = require("request");
const bodyParser = require("body-parser");
const https = require("https");

const app = express();
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
//app.use(https);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/success.html');
});


app.post('/', (req, res) => {
  const fname = req.body.fname;
  const lname = req.body.lname;
  const email = req.body.email;
  console.log(`my name is ${fname} ${lname}`);

  var data = {
    members:[
      {
        email_address: email,
        status:'subscribed',
        merge_fields: {
          FNAME:fname,
          LNAME:lname
        }
      }
    ]
  };

  const jsonData = JSON.stringify(data);
  const url = "https://us17.api.mailchimp.com/3.0/lists/0d21f5939f";
  const options = {
    method: "POST",
    auth: "alphatronexXXX:5a8d373814cf4b40c49b52cfdd9a62ce-us17"
  }

  const request = https.request(url, options, (response) => {
    if (response.statusCode !== 200) {
      res.sendFile(__dirname + '/failure.html');
    }

    response.on('data', (data) => {
      console.log(JSON.parse(data));
    });

    res.sendFile(__dirname + '/success.html');
  });

  request.write(jsonData);
  request.end();

});


app.listen(process.env.PORT || 3000, () => console.log('Server is running on port 3000'));


// API Key
// 5a8d373814cf4b40c49b52cfdd9a62ce-us17

// List
// 0d21f5939f
